<?php

namespace App\Tests;

use App\Entity\Rental;
use App\Repository\CustomerRepository;
use App\Repository\InventoryRepository;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Persistence\ObjectRepository;
use PHPUnit\Framework\TestCase;

class InventoryTest extends TestCase
{
    public function testSomething(): void
    {
        $this->assertTrue(true);
    }

    public function testObjectAssertion(InventoryRepository $inventoryRepository, CustomerRepository $customerRepository)
    {
        $film_id = 23;
        $inventory = $inventoryRepository->getInventoryByFilm($film_id)[0];
        $customer = $customerRepository->find(213);

        $rental = new Rental();
        $rental->setInventory($inventory);
        $rental->setCustomer($customer);
        $rental->setStaffId(1);
        $rental->setRentalDate(new \DateTime());
        $rental->setLastUpdate(new \DateTime());
        $rental->setReturnDate(new \DateTime());

        $inventoryRepository = $this->createMock(ObjectRepository::class);
        $inventoryRepository->expects($this->any())
            ->method('persist')
            ->willReturn($inventory);

        $objectManager = $this->createMock(ObjectManager::class);
        // use getMock() on PHPUnit 5.3 or below
        // $objectManager = $this->getMock(ObjectManager::class);
        $objectManager->expects($this->any())
            ->method('getRepository')
            ->willReturn($inventoryRepository);
    }
}
