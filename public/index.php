<?php

use App\Kernel;

require_once dirname(__DIR__).'/vendor/autoload_runtime.php';





//$kernel = new App\Kernel('dev', true);
//$request = Request::createFromGlobals();
//$response = $kernel->handle($request);
//$response->send();
//$kernel->terminate($request, $response);


return function (array $context) {
    return new Kernel($context['APP_ENV'], (bool) $context['APP_DEBUG']);
};


