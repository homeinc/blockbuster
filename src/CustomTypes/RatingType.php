<?php

namespace App\CustomTypes;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\Type;

class RatingType extends Type
{

    const RATINGTYPE = 'RatingType';

    private array $values = ['G','PG','PG-13','R','NC-17'];

    public function getSQLDeclaration(array $fieldDeclaration, AbstractPlatform $platform) : string
    {
        $values = array_map(function($value) {
            return "'" . $value . "'";
        }, $this->values);

        return "ENUM(" . implode(", ", $values) . ")";
    }

    public function convertToPHPValue($value, AbstractPlatform $platform):String
    {
        return (string) $value;
    }

    public function convertToDatabaseValue($value, AbstractPlatform $platform):String
    {
        if (!in_array($value, $this->values)) {
            throw new \InvalidArgumentException("Invalid value '" . $value . "' for ENUM");
        }

        return (string) $value;
    }

    public function requiresSQLCommentHint(AbstractPlatform $platform):bool
    {
        return true;
    }

    public function getName():string
    {
        return self::RATINGTYPE;
    }

    public function setValues(array $values)
    {
        $this->values = $values;
    }

}