<?php

namespace App\CustomTypes;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\Type;

class SpecialFeaturesType extends Type
{
    const SPECIALFEATURESTYPE = 'SpacialFeaturesType';

    private array $values = ['Trailers','Commentaries','Deleted Scenes','Behind the Scenes'];

    public function getSQLDeclaration(array $column, AbstractPlatform $platform):string
    {
        $values = array_map(function($value) {
            return "'" . $value . "'";
        }, $this->values);

        return "SET (" . implode(", ", $values) . ")";
    }

    public function getName():string
    {
        return self::SPECIALFEATURESTYPE;
    }

    public function convertToPHPValue($value, AbstractPlatform $platform):String
    {
        return (string) $value;
    }

    public function convertToDatabaseValue($value, AbstractPlatform $platform):String
    {
        if (!in_array($value, $this->values)) {
            throw new \InvalidArgumentException("Invalid value '" . $value . "' for SET");
        }

        return (string) $value;
    }

    public function requiresSQLCommentHint(AbstractPlatform $platform):bool
    {
        return true;
    }


    public function setValues(array $values)
    {
        $this->values = $values;
    }
}