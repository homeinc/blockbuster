<?php

namespace App\Service;

use App\Entity\Rental;
use App\Repository\CustomerRepository;
use App\Repository\InventoryRepository;
use App\Repository\RentalRepository;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Config\Definition\Exception\DuplicateKeyException;
use Symfony\Component\HttpFoundation\Request;

class RentalService
{
    public function __construct(protected RentalRepository $rentalRepository,
                                protected InventoryRepository $inventoryRepository,
                                protected CustomerRepository $customerRepository)
    {

    }

    public function store(Request $request)
    {
        $film_id = $request->request->get('film_id');
        $customer_id  = $request->request->get('customer_id');
        $inventory = $this->inventoryRepository->getInventoryByFilm($film_id)[0];

        $customer = $this->customerRepository->find($customer_id);
        $rental = new Rental($inventory, $customer, 1);

        try {
            $this->rentalRepository->save($rental, true);
        } catch (DuplicateKeyException $e) {
            return new DuplicateKeyException("Customer already has rental with this movie");
        }
    }

    public function view(int $customer_id):array
    {
        $rentals =   $this->rentalRepository->findBy(['customer' => $customer_id]);

        $results = [];
        foreach($rentals as $index=>$rental) {
            $results [$index]['title'] =$rental->getInventory()->getFilm()->getTitle();
            $results[$index]['rental_duration'] =$rental->getInventory()->getFilm()->getRentalDuration();
            $results[$index]['description'] =$rental->getInventory()->getFilm()->getDescription();
            $results[$index]['release_year'] =$rental->getInventory()->getFilm()->getReleaseYear();
            $results[$index]['rental_date'] = $rental->getRentalDate();
            $results[$index]['return_date'] = $rental->getReturnDate();
        }
        return $results;
//        dd($rentals[0]->getInventory()->getFilm()->getTitle());
    }
}