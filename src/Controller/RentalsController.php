<?php

namespace App\Controller;

use App\Repository\CustomerRepository;
use App\Repository\InventoryRepository;
use App\Repository\RentalRepository;
use App\Service\RentalService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Config\Definition\Exception\DuplicateKeyException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class RentalsController extends AbstractController
{

    protected RentalService $rentalService;
    public function __construct(RentalRepository $rentalRepository,
                                InventoryRepository $inventoryRepository,
                                CustomerRepository $customerRepository)
    {
        $this->rentalService = new RentalService( $rentalRepository, $inventoryRepository, $customerRepository);
    }

    #[Route('/rentals', name: 'add_rental', methods:['POST'])]
    public function store(Request $request)
    {
        try {
            $this->rentalService->store($request);
        } catch (DuplicateKeyException $duplicateKeyException) {
             return $this->redirectToRoute('app_film');
        }
        return $this->redirectToRoute('add_rental');
    }

    #[Route('/rentals', name: 'view_rentals', methods:['GET'])]
    public function index()
    {
        $user = $this->getUser();
        $user_id = $user->getId();
        $rentals = $this->rentalService->view($user_id);
       return $this->render('rentals/index.html.twig', ['rentals' => $rentals]);
    }
}
