<?php

namespace App\Controller;

use App\Repository\FilmRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class FilmController extends AbstractController
{
//    #[Route('/film', name: 'app_film', methods: ["GET"])]
    public function index(Request $request, FilmRepository $filmRepository  ): Response
    {
        $currentPage = $request->get('page') ?? 1;
        $result =$filmRepository->showFilmList(20, $currentPage);
        return $this->render('film/index.html.twig', [
            'films' =>$result['films'],
            'currentPage' => max($currentPage, 4),
            'totalItems' => $result['totalItems'],
            'totalPages' => min($result['totalPages'], 50)
        ]);
    }

    #[Route('/film', name: 'app_search', methods: ["POST"])]
    public function store(Request $request, FilmRepository $filmRepository, ValidatorInterface $validator)
    {
        $name=$request->request->get('searchInput');
        $result = $filmRepository->showFilmList(20, 1, $name);
        return $this->render('film/index.html.twig', [
            'films' =>$result['films'],
            'currentPage' => 1,
            'totalItems' => $result['totalItems'],
            'totalPages' => min($result['totalPages'], 50)
        ]);
    }
}
