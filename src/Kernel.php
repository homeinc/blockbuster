<?php

namespace App;

use App\CustomTypes\RatingType;
use App\CustomTypes\SpecialFeaturesType;
use Doctrine\DBAL\Types\Type;
use Symfony\Bundle\FrameworkBundle\Kernel\MicroKernelTrait;
use Symfony\Component\HttpKernel\Kernel as BaseKernel;

class Kernel extends BaseKernel
{
    use MicroKernelTrait;


    public function __construct(string $environment, bool $debug)
    {
        parent::__construct($environment, $debug);
        try {
            Type::addType(RatingType::RATINGTYPE, RatingType::class);
        } catch (\Doctrine\DBAL\Exception $e) {
            echo "Catch RatingType : " . $e->getMessage();
        }

        try {
            Type::addType(SpecialFeaturesType::SPECIALFEATURESTYPE, SpecialFeaturesType::class);
        } catch (\Doctrine\DBAL\Exception $e) {
            echo "Catch SpecialFeaturesType: " . $e->getMessage();
        }
    }
}
