<?php

namespace App\Entity;

use App\Repository\FilmRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Table(name : "film")]
#[ORM\Entity(repositoryClass: FilmRepository::class)]
class Film
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(name: 'id')]
    private ?int $id = null;

    #[ORM\Column(length: 100)]
    private ?string $title = null;

    #[ORM\Column(type: Types::TEXT)]
    private ?string $description = null;

    #[ORM\Column(length: 10)]
    private ?string $release_year = null;

    #[ORM\ManyToOne(inversedBy: 'films')]
    private ?Language $language = null;

    #[ORM\Column(type: Types::SMALLINT, nullable: true)]
    private ?int $original_language_id = null;

    #[ORM\Column(type: Types::SMALLINT)]
    private ?int $rental_duration = null;

    #[ORM\Column(type: Types::DECIMAL, precision: 5, scale: 2)]
    private ?string $rental_rate = null;

    #[ORM\Column(type: Types::SMALLINT)]
    private ?int $length = null;

    #[ORM\Column(type: Types::DECIMAL, precision: 5, scale: 2)]
    private ?string $replacement_cost = null;

    #[ORM\Column(type: 'RatingType')]
    private $rating = null;

    #[ORM\Column(type: 'SpacialFeaturesType')]
    private $special_features = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $last_update = null;

    #[ORM\OneToMany(mappedBy: 'film', targetEntity: FilmCategory::class)]
    private Collection $filmCategories;

    #[ORM\OneToMany(mappedBy: 'film', targetEntity: FilmActor::class)]
    private Collection $filmActors;

    #[ORM\OneToOne(mappedBy: 'film', cascade: ['persist', 'remove'])]
    private ?FilmText $filmText = null;

    #[ORM\OneToMany(mappedBy: 'film', targetEntity: Inventory::class)]
    private Collection $inventories;


    public function __construct()
    {
        $this->filmCategories = new ArrayCollection();
        $this->filmActors = new ArrayCollection();
        $this->inventories = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getReleaseYear(): ?string
    {
        return $this->release_year;
    }

    public function setReleaseYear(string $release_year): self
    {
        $this->release_year = $release_year;

        return $this;
    }

    public function getLanguage(): ?Language
    {
        return $this->language;
    }

    public function setLanguage(?Language $language): self
    {
        $this->language = $language;

        return $this;
    }

    public function getOriginalLanguageId(): ?int
    {
        return $this->original_language_id;
    }

    public function setOriginalLanguageId(?int $original_language_id): self
    {
        $this->original_language_id = $original_language_id;

        return $this;
    }

    public function getRentalDuration(): ?int
    {
        return $this->rental_duration;
    }

    public function setRentalDuration(int $rental_duration): self
    {
        $this->rental_duration = $rental_duration;

        return $this;
    }

    public function getRentalRate(): ?string
    {
        return $this->rental_rate;
    }

    public function setRentalRate(string $rental_rate): self
    {
        $this->rental_rate = $rental_rate;

        return $this;
    }

    public function getLength(): ?int
    {
        return $this->length;
    }

    public function setLength(int $length): self
    {
        $this->length = $length;

        return $this;
    }

    public function getReplacementCost(): ?string
    {
        return $this->replacement_cost;
    }

    public function setReplacementCost(string $replacement_cost): self
    {
        $this->replacement_cost = $replacement_cost;

        return $this;
    }

    public function getRating()
    {
        return $this->rating;
    }

    public function setRating($rating): self
    {
        $this->rating = $rating;

        return $this;
    }

    public function getSpecialFeatures()
    {
        return $this->special_features;
    }

    public function setSpecialFeatures($special_features): self
    {
        $this->special_features = $special_features;

        return $this;
    }

    public function getLastUpdate(): ?\DateTimeInterface
    {
        return $this->last_update;
    }

    public function setLastUpdate(?\DateTimeInterface $last_update): self
    {
        $this->last_update = $last_update;

        return $this;
    }

    /**
     * @return Collection<int, FilmCategory>
     */
    public function getFilmCategories(): Collection
    {
        return $this->filmCategories;
    }

    public function addFilmCategory(FilmCategory $filmCategory): self
    {
        if (!$this->filmCategories->contains($filmCategory)) {
            $this->filmCategories->add($filmCategory);
            $filmCategory->setFilm($this);
        }

        return $this;
    }

    public function removeFilmCategory(FilmCategory $filmCategory): self
    {
        if ($this->filmCategories->removeElement($filmCategory)) {
            // set the owning side to null (unless already changed)
            if ($filmCategory->getFilm() === $this) {
                $filmCategory->setFilm(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, FilmActor>
     */
    public function getFilmActors(): Collection
    {
        return $this->filmActors;
    }

    public function addFilmActor(FilmActor $filmActor): self
    {
        if (!$this->filmActors->contains($filmActor)) {
            $this->filmActors->add($filmActor);
            $filmActor->setFilm($this);
        }

        return $this;
    }

    public function removeFilmActor(FilmActor $filmActor): self
    {
        if ($this->filmActors->removeElement($filmActor)) {
            // set the owning side to null (unless already changed)
            if ($filmActor->getFilm() === $this) {
                $filmActor->setFilm(null);
            }
        }

        return $this;
    }

    public function getFilmText(): ?FilmText
    {
        return $this->filmText;
    }

    public function setFilmText(?FilmText $filmText): self
    {
        // unset the owning side of the relation if necessary
        if ($filmText === null && $this->filmText !== null) {
            $this->filmText->setFilm(null);
        }

        // set the owning side of the relation if necessary
        if ($filmText !== null && $filmText->getFilm() !== $this) {
            $filmText->setFilm($this);
        }

        $this->filmText = $filmText;

        return $this;
    }

    /**
     * @return Collection<int, Inventory>
     */
    public function getInventories(): Collection
    {
        return $this->inventories;
    }

    public function addInventory(Inventory $inventory): self
    {
        if (!$this->inventories->contains($inventory)) {
            $this->inventories->add($inventory);
            $inventory->setFilm($this);
        }

        return $this;
    }

    public function removeInventory(Inventory $inventory): self
    {
        if ($this->inventories->removeElement($inventory)) {
            // set the owning side to null (unless already changed)
            if ($inventory->getFilm() === $this) {
                $inventory->setFilm(null);
            }
        }

        return $this;
    }
}