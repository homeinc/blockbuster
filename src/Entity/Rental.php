<?php

namespace App\Entity;

use App\Repository\RentalRepository;
use DateInterval;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Table(name : "rental")]
#[ORM\Entity(repositoryClass: RentalRepository::class)]
class Rental
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column]
    private ?int $staff_id = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    private ?\DateTimeInterface $rental_date = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    private ?\DateTimeInterface $return_date = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    private ?\DateTimeInterface $last_update = null;

    #[ORM\ManyToOne(inversedBy: 'rentals')]
    private ?Inventory $inventory = null;

    #[ORM\ManyToOne(inversedBy: 'rentals')]
    private ?Customer $customer = null;


    public function __construct(Inventory $inventory, Customer $customer, int $staff_id = 1)
    {

        $this->setInventory($inventory);
        $this->setCustomer($customer);
        $this->setStaffId($staff_id);
        $this->setRentalDate(new \DateTime());
        $this->setLastUpdate(new \DateTime());

        $rentalDurationDays = $inventory->getFilm()->getRentalDuration();
        $rentalInterval = new DateInterval("P{$rentalDurationDays}D"); // P3D for 3 days
        $now = new \DateTime();
        $returnDate = $now->add($rentalInterval);
        $this->setReturnDate($returnDate);
    }
    public function getId(): ?int
    {
        return $this->id;
    }

    public function getStaffId(): ?int
    {
        return $this->staff_id;
    }

    public function setStaffId(int $staff_id): self
    {
        $this->staff_id = $staff_id;

        return $this;
    }

    public function getRentalDate(): \DateTimeInterface | string
    {
        return $this->rental_date->format("d-M-y H:i:s");
    }

    public function setRentalDate(\DateTimeInterface $rental_date): self
    {
        $this->rental_date = $rental_date;

        return $this;
    }

    public function getReturnDate(): \DateTimeInterface|string
    {
        return $this->return_date->format("d-M-y H:i:s");
    }

    public function setReturnDate(\DateTimeInterface $return_date): self
    {
        $this->return_date = $return_date;

        return $this;
    }

    public function getLastUpdate(): ?\DateTimeInterface
    {
        return $this->last_update;
    }

    public function setLastUpdate(\DateTimeInterface $last_update): self
    {
        $this->last_update = $last_update;

        return $this;
    }

    public function getInventory(): ?Inventory
    {
        return $this->inventory;
    }

    public function setInventory(?Inventory $inventory): self
    {
        $this->inventory = $inventory;

        return $this;
    }

    public function getCustomer(): ?Customer
    {
        return $this->customer;
    }

    public function setCustomer(?Customer $customer): self
    {
        $this->customer = $customer;

        return $this;
    }
}
