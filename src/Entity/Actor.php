<?php

namespace App\Entity;

use App\Repository\ActorRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Table(name : "actor")]
#[ORM\Entity(repositoryClass: ActorRepository::class)]
class Actor
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(name: 'id')]
    private ?int $id = null;

    #[ORM\Column(length: 50)]
    private ?string $first_name = null;

    #[ORM\Column(length: 50)]
    private ?string $last_name = null;

    #[ORM\Column(type: Types::TIME_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $last_update = null;

    #[ORM\OneToMany(mappedBy: 'actor', targetEntity: FilmActor::class)]
    private Collection $filmActors;

    public function __construct()
    {
        $this->filmActors = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFirstName(): ?string
    {
        return $this->first_name;
    }

    public function setFirstName(string $first_name): self
    {
        $this->first_name = $first_name;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->last_name;
    }

    public function setLastName(string $last_name): self
    {
        $this->last_name = $last_name;

        return $this;
    }

    public function getLastUpdate(): ?\DateTimeInterface
    {
        return $this->last_update;
    }

    public function setLastUpdate(?\DateTimeInterface $last_update): self
    {
        $this->last_update = $last_update;

        return $this;
    }

    /**
     * @return Collection<int, FilmActor>
     */
    public function getFilmActors(): Collection
    {
        return $this->filmActors;
    }

    public function addFilmActor(FilmActor $filmActor): self
    {
        if (!$this->filmActors->contains($filmActor)) {
            $this->filmActors->add($filmActor);
            $filmActor->setActor($this);
        }

        return $this;
    }

    public function removeFilmActor(FilmActor $filmActor): self
    {
        if ($this->filmActors->removeElement($filmActor)) {
            // set the owning side to null (unless already changed)
            if ($filmActor->getActor() === $this) {
                $filmActor->setActor(null);
            }
        }

        return $this;
    }
}
