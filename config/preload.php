<?php

if (file_exists(dirname(__DIR__).'/var/cache/prod/App_KernelProdContainer.preload.php')) {
    require dirname(__DIR__).'/var/cache/prod/App_KernelProdContainer.preload.php';
}

\Doctrine\DBAL\Types\Type::addType('Rating', \App\CustomTypes\RatingType::class);